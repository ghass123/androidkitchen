package com.androidtutorialshub.loginregister.utils;

import android.content.Context;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



/**
 * Created by YounasBangash on 11/21/2017.
 */

public class RetrofitBuilder {
    public RetrofitService retrofitService;
    private static Retrofit retrofit;

    public RetrofitBuilder(Context context) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://128.199.134.182")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        retrofitService = retrofit.create(RetrofitService.class);
    }
}
