package com.androidtutorialshub.loginregister.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by YounasBangash on 11/25/2017.
 */

public class PreferenceManager {
    private static final String PREF_NAME = "com.androidtutorialshub.loginregister";
    private static final String KEY_VALUE_LOGIN = "isLogin";
    private static final String KEY_VALUE_USERID = "userId";

    private static PreferenceManager sInstance;
    private final SharedPreferences mPref;

    private PreferenceManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferenceManager(context);
        }
    }

    public static synchronized PreferenceManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferenceManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public void setIsLogin(boolean key) {
        mPref.edit()
                .putBoolean(KEY_VALUE_LOGIN, key)
                .apply();
    }

    public String getUserID() {
        return mPref.getString(KEY_VALUE_USERID, "");
    }

    public void setUserID(String userID) {
        mPref.edit()
                .putString(KEY_VALUE_USERID, userID)
                .apply();
    }

    public boolean getIsLogin() {
        return mPref.getBoolean(KEY_VALUE_LOGIN, false);
    }


    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}
