package com.androidtutorialshub.loginregister.utils;

import android.app.Application;

/**
 * Created by YounasBangash on 11/25/2017.
 */

public class ApplicationClass extends Application {
    private RetrofitBuilder retrofit = null;
    public static RetrofitService retrofitService = null;

    @Override
    public void onCreate() {
        super.onCreate();
        retrofit = new RetrofitBuilder(this);
        retrofitService = retrofit.retrofitService;
        PreferenceManager.initializeInstance(this);
    }
}
