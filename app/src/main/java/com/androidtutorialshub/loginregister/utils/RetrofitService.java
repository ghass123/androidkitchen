package com.androidtutorialshub.loginregister.utils;

import com.androidtutorialshub.loginregister.model.Ingredients;
import com.androidtutorialshub.loginregister.model.Recipe;
import com.androidtutorialshub.loginregister.model.SuccessModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by YounasBangash on 11/21/2017.
 */

@SuppressWarnings("SpellCheckingInspection")
public interface RetrofitService {

        @GET("/udacity/project_test/register_new_user.php?")
        Call<SuccessModel> registerUser(@Query("userName") String userName,
                                        @Query("userEmail") String userEmail,
                                        @Query("userPassword") String userPassword);

        @GET("/udacity/project_test/login_user.php?")
        Call<SuccessModel> loginUser(@Query("user_email") String userEmail,
                                     @Query("user_pwd") String userPassword);

        @GET("/udacity/project_test/get_all_ingredients.php?")
        Call<List<Ingredients>> getItemList(@Query("user_id") String user_id);

        @GET("/udacity/project_test/get_all_reccipe_list.php")
        Call<List<Recipe>> getAllRecipes();

}
