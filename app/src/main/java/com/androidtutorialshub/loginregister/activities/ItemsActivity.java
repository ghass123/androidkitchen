package com.androidtutorialshub.loginregister.activities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.adapters.ItemRecyclerAdapter;
import com.androidtutorialshub.loginregister.model.Ingredients;
import com.androidtutorialshub.loginregister.utils.BasicActivity;
import com.androidtutorialshub.loginregister.utils.Logger;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.androidtutorialshub.loginregister.utils.ApplicationClass.retrofitService;

public class ItemsActivity extends BasicActivity {

//    ListView listview;

    RecyclerView recyclerViewUsers = null;
    ItemRecyclerAdapter itemRecyclerAdapter = null;
    RecyclerView.LayoutManager mLayoutManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);

        recyclerViewUsers = (RecyclerView) findViewById(R.id.recyclerItemList);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        if(isNetworkConnected())
            getItemList("1");
        else
            Toast.makeText(this, "Internet Connection Required",
                    Toast.LENGTH_SHORT).show();


    }

    private void getItemList(final String useid) {
        showProgressDialog("Fetching List...");
        Call<List<Ingredients>> jsonDataClass = retrofitService.getItemList(useid);
        jsonDataClass.enqueue(new Callback<List<Ingredients>>() {
            @Override
            public void onResponse(Call<List<Ingredients>> call, Response<List<Ingredients>> response) {
                Logger.d("onResponse : " + response.toString());
                List<Ingredients> ingredientsList = response.body();
                itemRecyclerAdapter = new ItemRecyclerAdapter(ingredientsList);
                recyclerViewUsers.setLayoutManager(mLayoutManager);
                recyclerViewUsers.setAdapter(itemRecyclerAdapter);
                hideProgressDialog();

            }

            @Override
            public void onFailure(Call<List<Ingredients>> call, Throwable t) {
                hideProgressDialog();
                Logger.d("onFailure : " + t.getLocalizedMessage());
                Toast.makeText(ItemsActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

