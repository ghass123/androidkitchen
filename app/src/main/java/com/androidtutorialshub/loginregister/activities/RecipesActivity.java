package com.androidtutorialshub.loginregister.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.adapters.ItemRecyclerAdapter;
import com.androidtutorialshub.loginregister.adapters.RecipeRecyclerAdapter;
import com.androidtutorialshub.loginregister.model.Ingredients;
import com.androidtutorialshub.loginregister.model.Recipe;
import com.androidtutorialshub.loginregister.utils.ApplicationClass;
import com.androidtutorialshub.loginregister.utils.BasicActivity;
import com.androidtutorialshub.loginregister.utils.Logger;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.androidtutorialshub.loginregister.utils.ApplicationClass.retrofitService;

public class RecipesActivity extends BasicActivity {
    private ProgressDialog mProgressDialog = null;
    private RecyclerView listRecipes = null;
    private RecipeRecyclerAdapter recipeRecyclerAdapter = null;
    private RecyclerView.LayoutManager mLayoutManager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);
        listRecipes = (RecyclerView) findViewById(R.id.itemsListViewRecipes);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());

        if(isNetworkConnected())
            getAllRecipes();
        else
            Toast.makeText(this, "Internet Connection Required",
                    Toast.LENGTH_SHORT).show();
    }


    private void getAllRecipes() {
        showProgressDialog("Fetching Recipes...");
        Call<List<Recipe>> jsonDataClass = retrofitService.getAllRecipes();
        jsonDataClass.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                Logger.d("onResponse : " + response.toString());
                List<Recipe> recipeList = response.body();
                recipeRecyclerAdapter = new RecipeRecyclerAdapter(recipeList);
                listRecipes.setLayoutManager(mLayoutManager);
                listRecipes.setAdapter(recipeRecyclerAdapter);
                hideProgressDialog();

            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                hideProgressDialog();
                Logger.d("onFailure : " + t.getLocalizedMessage());
                Toast.makeText(RecipesActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
