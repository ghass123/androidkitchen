package com.androidtutorialshub.loginregister.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.utils.PreferenceManager;

public class SplashScreenActivity extends AppCompatActivity {
    private PreferenceManager preferenceManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        preferenceManager = PreferenceManager.getInstance();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(preferenceManager.getIsLogin()){
                    startActivity(new Intent(SplashScreenActivity.this,UsersListActivity.class));
                }else{
                    startActivity(new Intent(SplashScreenActivity.this,LoginActivity.class));
                }
                finish();
            }
        }, 2000);
    }
}
