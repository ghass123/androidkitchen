package com.androidtutorialshub.loginregister.helpers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.activities.ItemsActivity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Dell on 11/14/2017.
 */

public class MyService extends Service {
    private Timer timer;
    @Override
    public void onCreate() {
// Code that's executed once when the service is created.
        Log.d("Myservice", "Service Created");
        startTimer();
    }
    @Override
    public int onStartCommand(Intent intent, int flags,
                              int startId) {
// Code that's executed each time another component
// starts the service by calling the startService method.
        return START_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onDestroy() {
// Code that's executed once when the service
// is no longer in use and is being destroyed.
        Log.d("Myservice", "Service Destroyed");
        stopTimer();
    }
    private void startTimer(){
        TimerTask task= new TimerTask(){
            @Override
            public void run(){
                Log.d("Myservice"," Look into the distance. It’s good for your eyes!");
                sendNotification(" Look into the distance. It’s good for your eyes!");
            }
        };
        timer= new Timer(true);
        int delay= 1000*10; //  INCREASE TIME

        int interval= 1000*10; // INCREASE TIME
        timer.schedule(task,delay,interval);
    }
    private void stopTimer(){
        if(timer!=null){
            timer.cancel();
        }
    }

    private void sendNotification(String text) {
// create the intent for the notification
        Intent notificationIntent = new Intent(this,
                ItemsActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
// create the pending intent
        int flags = PendingIntent.FLAG_UPDATE_CURRENT;
        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        this, 0, notificationIntent, flags);
// create the variables for the notification
        int icon = R.drawable.ic_launcher;
        CharSequence tickerText = "Look into the distance. It’s good for your eyes!";
        CharSequence contentTitle = getText(R.string.app_name);
        CharSequence contentText = text;
// create the notification and set its data

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setTicker(tickerText)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
// display the notification
        NotificationManager manager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        final int NOTIFICATION_ID = 1;
        manager.notify(NOTIFICATION_ID, notification);
    }
}
