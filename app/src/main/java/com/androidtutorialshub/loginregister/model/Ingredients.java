package com.androidtutorialshub.loginregister.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 11/25/2017.
 */

public class Ingredients {

    @SerializedName("INGREDIENT_ID")
    public String INGREDIENT_ID;
    @SerializedName("INGREDIENT_DESC")

    public String INGREDIENT_DESC;
    @SerializedName("UNIT")
    public String UNIT;

    @SerializedName("TAG_ID")
    public String TAG_ID;

    @SerializedName("CUSTOMER_ID")
    public String CUSTOMER_ID;

    @SerializedName("EXPIRAY_DATE")
    public String EXPIRAY_DATE;
}
