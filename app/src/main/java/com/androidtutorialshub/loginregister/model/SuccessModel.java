package com.androidtutorialshub.loginregister.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by YounasBangash on 11/25/2017.
 */

@SuppressWarnings("SpellCheckingInspection")
public class SuccessModel {
    @SerializedName("success")
    public String success;
    @SerializedName("message")
    public String message;
    @SerializedName("userid")
    public String userID;
}
