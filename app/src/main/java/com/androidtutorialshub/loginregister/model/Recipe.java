package com.androidtutorialshub.loginregister.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 11/25/2017.
 */

public class Recipe {

    @SerializedName("RECIPE_ID")
    public String RECIPE_ID;

    @SerializedName("RECIPE_NAME")
    public String RECIPE_NAME;

    @SerializedName("TIME_TO_PREPARE")
    public String TIME_TO_PREPARE;

    @SerializedName("NUMBER_TO_SERVINGS")
    public String NUMBER_TO_SERVINGS;

    @SerializedName("CALORIES_PER_SERVING")
    public String CALORIES_PER_SERVING;

    @SerializedName("NUTRITIONAL_INFORMATION")
    public String INSTRUCTIONS;
}
