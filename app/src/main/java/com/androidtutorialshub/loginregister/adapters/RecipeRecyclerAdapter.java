package com.androidtutorialshub.loginregister.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidtutorialshub.loginregister.R;
import com.androidtutorialshub.loginregister.model.Ingredients;
import com.androidtutorialshub.loginregister.model.Recipe;

import java.text.MessageFormat;
import java.util.List;

/**
 * Created by lalit on 10/10/2016.
 */

public class RecipeRecyclerAdapter extends RecyclerView.Adapter<RecipeRecyclerAdapter.UserViewHolder> {

    private List<Recipe> listRecipes;

    public RecipeRecyclerAdapter(List<Recipe> listIngredients) {
        this.listRecipes = listIngredients;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler, parent, false);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.itemTitle.setText(listRecipes.get(position).RECIPE_NAME);
        holder.itemExpDate.setText(MessageFormat.format("Preparation Time :{0}",
                listRecipes.get(position).TIME_TO_PREPARE));
    }

    @Override
    public int getItemCount() {
//        Log.v(ItemRecyclerAdapter.class.getSimpleName(),""+listUsers.size());
        return listRecipes.size();
    }


    /**
     * ViewHolder class
     */
    public class UserViewHolder extends RecyclerView.ViewHolder {

        public TextView itemTitle;
        public TextView itemExpDate;

        public UserViewHolder(View view) {
            super(view);
            itemTitle = (TextView) view.findViewById(R.id.itemDescp);
            itemExpDate = (TextView) view.findViewById(R.id.itemExpDate);

        }
    }
}
